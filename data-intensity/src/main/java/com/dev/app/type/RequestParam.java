package com.dev.app.type;

public class RequestParam {
    private Long duration;
    private Integer threadsCount;

    public RequestParam() {
    }

    public RequestParam(Long duration, Integer threadsCount) {
        this.duration = duration;
        this.threadsCount = threadsCount;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Integer getThreadsCount() {
        return threadsCount;
    }

    public void setThreadsCount(Integer threadsCount) {
        this.threadsCount = threadsCount;
    }

    public Boolean validation() {
        if (duration != null && duration < 0) {
            return false;
        }
        if (threadsCount != null && threadsCount < 0) {
            return false;
        }
        return true;
    }
}
