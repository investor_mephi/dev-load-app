package com.dev.app.controller;

import com.dev.app.service.DataService;
import com.dev.app.type.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("")
public class DataController {

    @Autowired
    DataService dataService;

    @RequestMapping(value = "/database", method = RequestMethod.POST)
    public ResponseEntity<Void> dataLoad(@RequestBody RequestParam requestParam) {
        if (requestParam == null || !requestParam.validation()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            dataService.dataLoad(requestParam.getDuration(), requestParam.getThreadsCount());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
