package com.dev.app;

import com.dev.app.actuator.service.AverageDelayCheck;
import com.dev.app.initialization.InitDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class Application {

    @Autowired
    AverageDelayCheck checkDelay;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        context.getBean(InitDataBase.class).init();
    }


    @PostConstruct
    public void startService() {
        checkDelay.start();
    }

}
