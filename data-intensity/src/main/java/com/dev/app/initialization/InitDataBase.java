package com.dev.app.initialization;

import com.dev.app.dao.DataRepository;
import com.dev.app.domain.DataObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class InitDataBase {

    @Value("${DEFAULT_COUNT_RECORDS}")
    private Integer DEFAULT_COUNT_RECORDS;

    @Autowired
    DataRepository dataRepository;

    public void init() {
        Long tableSize = dataRepository.count();

        if (tableSize == 0) {
            for (int i = 0; i < DEFAULT_COUNT_RECORDS; i++) {
                dataRepository.save(new DataObject().ofDefault());
            }
        }
    }

}
