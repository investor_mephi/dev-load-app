package com.dev.app.service;


public interface DataService {
    void dataLoad(Long duration, Integer threadsCount) throws InterruptedException;
}
