package com.dev.app.service;

import com.dev.app.dao.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class DataServiceImpl implements DataService {

    @Value("${DEFAULT_COUNT_RECORDS}")
    private Integer DEFAULT_COUNT_RECORDS;
    @Value("${DEFAULT_DURATION}")
    private Long DEFAULT_DURATION;
    @Value("${DEFAULT_THREAD_COUNT}")
    private Integer DEFAULT_THREAD_COUNT;

    @Autowired
    DataRepository dataRepository;

    @Override
    public void dataLoad(final Long duration, final Integer threadsCount) throws InterruptedException {

        final Integer THREAD_COUNT = (threadsCount == null) ? DEFAULT_THREAD_COUNT : threadsCount;
        final Long DURATION = (duration == null) ? DEFAULT_DURATION : duration;

        ExecutorService executor = Executors.newFixedThreadPool(THREAD_COUNT);

        List<Runnable> workerThreadList = new LinkedList<>();
        for (int i = 0; i <= DEFAULT_COUNT_RECORDS; i++) {
            Runnable worker = new WorkerThread(new Long(i), dataRepository);
            workerThreadList.add(worker);
        }

        Long start = System.currentTimeMillis();

        while (!checkDuration(start, DURATION)) {
            for (Runnable w : workerThreadList) {
                executor.execute(w);
                if (checkDuration(start, DURATION)) {
                    break;
                }
            }
        }
        executor.shutdownNow();
    }

    private Boolean checkDuration(Long start, Long duration) {
        return ((System.currentTimeMillis() - start) >= duration * 1000);
    }
}

class WorkerThread implements Runnable {
    private Long dataObjectId;
    private DataRepository dataRepository;

    public WorkerThread(Long dataObjectId, DataRepository dataRepository) {
        this.dataRepository = dataRepository;
        this.dataObjectId = dataObjectId;
    }

    @Override
    public void run() {
        //System.out.println(Thread.currentThread().getName() + " Start. Id = " + dataObjectId);
        dataRepository.updateById(new Long(dataObjectId));
        //System.out.println(Thread.currentThread().getName() + " End.");
    }
}