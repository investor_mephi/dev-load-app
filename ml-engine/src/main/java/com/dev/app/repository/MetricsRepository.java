package com.dev.app.repository;

import com.dev.app.model.MetricsEntity;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import java.time.LocalDateTime;
import java.util.Collection;

public interface MetricsRepository extends CassandraRepository<MetricsEntity> {

    @Query("Select * from Metric where metricTimestamp>=?0 and metricTimestamp<=?1 ALLOW FILTERING")
    Collection<MetricsEntity>  findByDataRange(LocalDateTime beginDate, LocalDateTime endDate);

    @Query("Select * from Metric where metricTimestamp>=?0 ALLOW FILTERING")
    Collection<MetricsEntity> findByDataRangeTest(LocalDateTime startTime);
}

//    @AllowFiltering
//    MetricsEntity findMetricsEntityByMetricTimestampBetween(Date begin, Date end)

