package com.dev.app.scheduler.tasks;

import com.dev.app.model.MetricsEntity;
import com.dev.app.service.AnalysisService;
import com.dev.app.type.DeployMessage;
import com.dev.app.web.DeployManagerClient;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.DateTimeFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class MetricsAnalysis implements Task {

    @Autowired
    private DeployManagerClient deployManagerClient;

    @Autowired
    private AnalysisService analysisService;

    @Value("${cron.analysis.metrics}")
    private String cron;

    ExecutorService executor = Executors.newFixedThreadPool(10);

    @PostConstruct
    public void init() {

    }

    public void execute() throws Exception {
        Collection<MetricsEntity> metricsEntities = analysisService.getMetrics();

        Collection<DeployMessage> deployMessages = analysisService.checkMetrics(metricsEntities);

        deployMessages.forEach(deployMessage ->
                executor.execute(() -> {
                    try {
                        deployManagerClient.sendTask(deployMessage);
                    } catch (UnsupportedEncodingException e) {
                        System.out.println("Can not send deployMessage. " + deployMessage.toString());
                    }
                    System.out.println("Sent: " + deployMessage.toString());
                })
        );
        executor.shutdown();
    }


    public boolean isAppliable() {
        CronSequenceGenerator generator = new CronSequenceGenerator(cron);
        DateTime dateTime = new DateTime();
        DateTime cronTime = new DateTime(generator.next(dateTime.minusSeconds(1).toDate()));
        DateTimeComparator comparator = DateTimeComparator.getInstance(DateTimeFieldType.secondOfMinute());
        return comparator.compare(dateTime, cronTime) == 0;
    }
}
