package com.dev.app.scheduler;

import com.dev.app.scheduler.tasks.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


import java.util.List;

@Component
public class TaskHandler {

    @Autowired
    private List<Task> sheduledTasks;

    @Scheduled(cron = "${cron.base}")
    public void tick() throws Exception {
        for (Task a: sheduledTasks) {
            if (a.isAppliable()) {
                a.execute();
            }
        }
        System.out.println("1 second");
    }
}