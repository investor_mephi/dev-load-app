package com.dev.app.service;

import com.dev.app.model.MetricsEntity;
import com.dev.app.repository.MetricsRepository;
import com.dev.app.type.DeployMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@Service
public class AnalysisService {

    @Autowired
    private MetricsRepository metricsRepository;

    @Value("${max.cpu.usage.percent}")
    private Integer maxCpuUsagePercent;

    public Collection<MetricsEntity> getMetrics() {

        LocalDateTime beginTime = LocalDateTime.now().minusSeconds(59);
        LocalDateTime endTime = LocalDateTime.now();

        return metricsRepository.findByDataRange(beginTime, endTime);
    }

    public Collection<DeployMessage> checkMetrics(final Collection<MetricsEntity> metricsEntities) {

        Collection<DeployMessage> deployMessages = new ArrayList<>();

        for (MetricsEntity metric : metricsEntities) {
            if (metric.getMetricValue() > maxCpuUsagePercent) {
                DeployMessage deployMessage = new DeployMessage();
                deployMessage.setIp("samos.dozen.mephi.ru");
                deployMessage.setPort(9028L);
                deployMessage.setPassword("dozenhpc");
                deployMessage.setImageName(getImageName(metric.getContainerName()));
                deployMessage.setContainerName(metric.getContainerName());
                deployMessage.setIpDest("samos.dozen.mephi.ru");
                deployMessage.setPortDest(9029L);
                deployMessage.setPasswordDest("dozenhpc");
                deployMessage.setIpDestInInternalNetwork("192.168.12.89");
                deployMessages.add(deployMessage);
            }
        }

        return deployMessages;
    }

    private String getImageName(final String containerName) {
        if (containerName.contains("computing")) {
            return "computing:spring";
        }
        if (containerName.contains("data")) {
            return "data:spring";
        }
        if (containerName.contains("hybrid")) {
            return "hybrid:spring";
        }
        return null;
    }

}
