
package com.dev.app.web;

import com.dev.app.service.AnalysisService;
import com.dev.app.type.DeployMessage;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.io.UnsupportedEncodingException;


@Service
public class DeployManagerClient {

    @Value("${deploy-manager.enabled}")
    private Boolean enabled;

    @Value("${deploy-manager.host}")
    private String host;

    @Value("${deploy-manager.port}")
    private String port;

    @Autowired
    private AnalysisService analysisService;


    public Boolean sendTask(DeployMessage deployMessage) throws UnsupportedEncodingException{
        if (enabled) {
            HttpClient httpClient = HttpClientBuilder.create().build();

            HttpPost httpPost = new HttpPost("http://" + host + ":" + port + "/deploy");

            Gson gson = new Gson();
            httpPost.setEntity(new StringEntity(gson.toJson(deployMessage, DeployMessage.class)));
            httpPost.addHeader("Content-Type", "application/json");
            try {
                HttpResponse response = httpClient.execute(httpPost);
                if (response.getStatusLine().getStatusCode() == 200) {
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
}
