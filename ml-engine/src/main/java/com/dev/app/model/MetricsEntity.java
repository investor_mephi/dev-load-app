package com.dev.app.model;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

import java.io.Serializable;
import java.util.Date;

@Table("Metric")
public class MetricsEntity implements Serializable {

    @PrimaryKeyColumn(name = "testId", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private Long testId;

    @Column(value = "metricName")
    private String metricName;

    @Column(value = "metricIpAndPort")
    private String metricIpAndPort;

    @Column(value = "hostName")
    private String hostName;

    @Column(value = "metricValue")
    private Double metricValue;

    @Column(value = "containerName")
    private String containerName;

    @PrimaryKeyColumn(name = "metricTimestamp", type = PrimaryKeyType.CLUSTERED)
    private Date metricTimestamp;

    public MetricsEntity(Long testId, String metricName, String metricIpAndPort, String hostName, Double metricValue, String containerName, Date metricTimestamp) {
        this.testId = testId;
        this.metricName = metricName;
        this.metricIpAndPort = metricIpAndPort;
        this.hostName = hostName;
        this.metricValue = metricValue;
        this.containerName = containerName;
        this.metricTimestamp = metricTimestamp;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public String getMetricName() {
        return metricName;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }

    public String getMetricIpAndPort() {
        return metricIpAndPort;
    }

    public void setMetricIpAndPort(String metricIpAndPort) {
        this.metricIpAndPort = metricIpAndPort;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public Double getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(Double metricValue) {
        this.metricValue = metricValue;
    }

    public Date getMetricTimestamp() {
        return metricTimestamp;
    }

    public void setMetricTimestamp(Date metricTimestamp) {
        this.metricTimestamp = metricTimestamp;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }
}