package com.dev.app.actuator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayDeque;
import java.util.Deque;

@Service
public class AverageDelayCheck {

    @Autowired
    Repository repository;

    @Value("${DEFAULT_HISTORY_SIZE}")
    private Integer size;

    private Deque<Double> deque = new ArrayDeque<>();

    public void start() {
        Thread thread = new Thread(this::run);
        thread.setDaemon(true);
        thread.setName("DbChecker");
        thread.start();
    }


    private void run() {
        while (true) {
            double currentDelay = check();
            if (deque.size() > size) {
                deque.pollLast();
            } else if (currentDelay == -1) {
                deque.clear();
            } else {
                deque.push(currentDelay);
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    public double getAverage() {
        return deque.stream().mapToDouble(i -> i / 1_000_000).average().orElse(-1);
    }

    private double check() {
        long start = System.nanoTime();
        String res = repository.check();
        return res == null ? -1 : System.nanoTime() - start;
    }

    public Integer getSizeHistory() {
        return size;
    }
}
