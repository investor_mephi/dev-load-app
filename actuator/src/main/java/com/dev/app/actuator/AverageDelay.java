package com.dev.app.actuator;

import com.dev.app.actuator.service.AverageDelayCheck;
import com.dev.app.actuator.utils.Numbers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.PublicMetrics;
import org.springframework.boot.actuate.metrics.Metric;
import org.springframework.stereotype.Component;
import java.util.Collection;
import java.util.LinkedHashSet;

@Component
public class AverageDelay implements PublicMetrics {

    @Autowired
    AverageDelayCheck check;

    @Override
    public Collection<Metric<?>> metrics() {
        Collection<Metric<?>> result = new LinkedHashSet<>();

        double preparedAverage = Numbers.roundAvoid(check.getAverage(),3);

        result.add(new Metric<>("database.average.latency", preparedAverage));
        return result;
    }
}
