package com.dev.app.actuator;

import com.dev.app.actuator.service.DelayCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLatency implements HealthIndicator {

    @Autowired
    DelayCheck delayCheck;

    @Override
    public Health health() {
        return Health
                .status(delayCheck.getLatencyStatus())
                .build();
    }
}
