package com.dev.app.actuator.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DelayCheck {

    private final String STATUS_RED = "RED";
    private final String STATUS_YELLOW = "YELLOW";
    private final String STATUS_GREEN = "GREEN";

    @Autowired
    Repository repository;

    @Value("${LATENCY_RED}")
    private Integer LATENCY_RED;

    @Value("${LATENCY_YELLOW}")
    private Integer LATENCY_YELLOW;

    public String getLatencyStatus() {
        double latency;
        try {
            latency = check();
        } catch (Exception e) {
            return STATUS_RED;
        }
        if (latency == -1 || latency > LATENCY_RED) {
            return STATUS_RED;
        }
        if (latency > LATENCY_YELLOW) {
            return STATUS_YELLOW;
        }
        return STATUS_GREEN;
    }

    public double check() {
        long start = System.nanoTime();
        String res = repository.check();
        return res == null ? -1 : (System.nanoTime() - start) / 1_000_000;
    }
}
