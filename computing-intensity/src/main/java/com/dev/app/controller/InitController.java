package com.dev.app.controller;

import com.dev.app.service.id.ServiceId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/service")
public class InitController {

    @Autowired
    ServiceId serviceId;

    @RequestMapping("/id")
    public String getId() {
        return serviceId.getId();
    }

    @RequestMapping("/stop")
    public void stop() {
        serviceId.sendQuery("http://localhost:9200/dozenhpc/timemetrics/", "STOP");
    }

}
