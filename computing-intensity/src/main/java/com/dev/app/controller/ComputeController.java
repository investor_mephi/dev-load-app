package com.dev.app.controller;

import com.dev.app.service.ComputeService;
import com.dev.app.service.id.Body;
import com.dev.app.service.id.ServiceId;
import com.dev.app.type.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("")
public class ComputeController {

    @Autowired
    ComputeService computeService;

    @Autowired
    ServiceId serviceId;

    @RequestMapping(method = RequestMethod.POST, value = "/computing")
    public ResponseEntity<String> startCompute(@RequestBody RequestParam requestParam) {
        if (requestParam == null || !requestParam.validation()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            computeService.compute(requestParam.getDuration(), requestParam.getThreadsCount());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

//    @RequestMapping(method = RequestMethod.POST,
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE,
//            value = "/dozenhpc/timemetricsMock/")
//    public ResponseEntity<String> elasticMock(@RequestBody Body body) {
//        //Захардкожено под старт. Запросы шлются с контейнера на узел и с узла на узел эластика,
//        // но с контейнера на узел эластика - нет. Данный эндпойнт просто принимает запрос и шлёт в эластик.
//
//
//        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
//        Map map = new HashMap<String, String>();
//        map.put("Content-Type", "application/json");
//
//        headers.setAll(map);
//
////        Body body = new Body(new Timestamp(Long.parseLong(request.getParameter("timestamp"))),
////                request.getParameter("action"),
////                request.getParameter("sequenceId"),
////                request.getParameter("ip"));
//
//        HttpEntity<?> request2 = new HttpEntity<>(body, headers);
//        try {
//            RestTemplate restTemplate = new RestTemplate();
//            //чтобы не ждать если не можем отправить
//            ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(1000);
//            ResponseEntity<String> response = restTemplate.postForEntity(
//                    "http://192.168.12.88:9200/dozenhpc/timemetrics/", request2, String.class);
//            String entityResponse = response.getBody();
//            System.out.println(entityResponse);
//        } catch (Exception e) {
//            //ignore
//            System.out.println("CANNOT SEND QUERY. ");
//        }
//
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

}
