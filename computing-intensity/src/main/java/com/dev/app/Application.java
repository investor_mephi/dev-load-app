package com.dev.app;

import com.dev.app.service.id.ServiceId;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.net.URISyntaxException;
import java.util.Date;

@SpringBootApplication
public class Application {

    private static Logger logger  = Logger.getLogger(Application.class);

    @Autowired
    ServiceId serviceId;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    private void prepareExitHandler () {
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run () {
                logger.warn("STOP");
                serviceId.sendQuery("http://localhost:9200/dozenhpc/timemetrics/", "STOP");
//                serviceId.sendQuery("http://localhost:9200/dozenhpc/timemetricsMock/", "STOP");
                System.out.println("SHUTDOWN HOOK");
                // application exit code here
            }
        }));
    }

    @PostConstruct
    public void init() throws URISyntaxException {
        logger.warn("START");
        prepareExitHandler();
        serviceId.sendQuery("http://localhost:9200/dozenhpc/timemetrics/", "START");
//        serviceId.sendQuery("http://localhost:9200/dozenhpc/timemetricsMock/", "START");
    }

}
