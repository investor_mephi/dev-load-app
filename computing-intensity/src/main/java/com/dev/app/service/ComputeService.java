package com.dev.app.service;

public interface ComputeService {
    void compute(Long duration, Integer threadsCount) throws InterruptedException;
}
