package com.dev.app.service.id;

import com.dev.app.Application;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class ServiceId {

    private static Logger logger = Logger.getLogger(Application.class);

    public String getId() {
        try {
            Optional<String> id = new BufferedReader(new FileReader("application_id")).lines().findFirst();
            if (id.isPresent()) {
                return id.get();
            } else {
                throw new RuntimeException("not found");
            }
        } catch (Exception e) {
            throw new RuntimeException("Can't get ID", e);
        }
    }

    public void generateId() {
        File f = new File("application_id");
        if (f.exists()) {
            return;
        }
        try (FileWriter write = new FileWriter(f)) {
            f.createNewFile();
            System.out.println("GENERATED");
            write.write(String.valueOf(new Double(Math.random() * 5000).intValue()));
        } catch (IOException e) {
            throw new RuntimeException("Error during generate  ID", e);
        }
    }


    public void sendQuery(String url, String type) {
        generateId();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();
        map.put("Content-Type", "application/json");

        headers.setAll(map);

        String ip = "UnknownIp";
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {

        }
        Body body = new Body(Timestamp.valueOf(LocalDateTime.now()), type, getId(), ip);

        HttpEntity<?> request = new HttpEntity<>(body, headers);

        logger.warn(type + body.toString());
        try {
            RestTemplate restTemplate = new RestTemplate();
            //чтобы не ждать если не можем отправить
            ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(1000);
            ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);
            String entityResponse = response.getBody();
            System.out.println(entityResponse);
        } catch (Exception e) {
            //ignore
            System.out.println("CANNOT SEND QUERY. ");
        }
    }


}
