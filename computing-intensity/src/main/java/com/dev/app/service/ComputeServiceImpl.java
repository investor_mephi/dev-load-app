package com.dev.app.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class ComputeServiceImpl implements ComputeService {


    @Value("${DEFAULT_DURATION}")
    private Long DEFAULT_DURATION;
    @Value("${DEFAULT_THREAD_COUNT}")
    private Integer DEFAULT_THREAD_COUNT;

    @Override
    public void compute(Long duration, Integer threadsCount) throws InterruptedException {

        final Integer THREAD_COUNT = (threadsCount == null) ? DEFAULT_THREAD_COUNT : threadsCount;
        final Long DURATION = (duration == null) ? DEFAULT_DURATION : duration;

        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);

        Collection<Runnable> tasks = new ArrayList<>();
        for (int i = 0; i < THREAD_COUNT; i++) {
            tasks.add(this::longCompute);
        }

        tasks.forEach(executorService::submit);

        new Thread(() -> {
            checkStatus(DURATION, executorService);
        }).start();


    }


    private void longCompute() {
        while (true) {
            if (isPrime()) {
                break;
            }
        }
    }

    private static boolean isPrime() {
        int n = 100000000;
        for (int i = 2; i * i <= n; i++) {
            if (Thread.currentThread().isInterrupted()) {
                return true;
            }
            if (n % i == 0)
                return false;
        }
        return false;
    }

    private void checkStatus(Long duration, ExecutorService service) {
        Long startTime = System.currentTimeMillis();
        while (startTime + duration * 1000 > System.currentTimeMillis()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                //ignore
            }
        }
        service.shutdownNow();
    }


}
