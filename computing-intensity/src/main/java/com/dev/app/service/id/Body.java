package com.dev.app.service.id;

import java.sql.Timestamp;

public class Body {

    Timestamp timestamp;
    String action;
    String sequenceId;
    String ip;

    public Body() {
    }

    public Body(Timestamp timestamp, String action, String sequenceId, String ip) {
        this.timestamp = timestamp;
        this.action = action;
        this.sequenceId = sequenceId;
        this.ip = ip;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public String getAction() {
        return action;
    }

    public String getSequenceId() {
        return sequenceId;
    }

    public String getIp() {
        return ip;
    }

    @Override
    public String toString() {
        return "Body{" +
                "timestamp=" + timestamp +
                ", action='" + action + '\'' +
                ", sequenceId='" + sequenceId + '\'' +
                ", ip='" + ip + '\'' +
                '}';
    }
}
