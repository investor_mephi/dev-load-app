### API ###

/computing   
{
	"duration": 10,  
	"threadsCount": 1  
}  
  
/database   
{
	"duration": 10,  
	"threadsCount": 1  
}

/hybrid  
{
	"duration": 10,  
	"threadsCount": 2,  
	"computingPercent": 50  
}



### Build images ###

cd /dev-load-app/
mvn clean install

mvn clean package -DskipTests

docker-compose build //Все три контейнера
docker-compose build compute-intensity //Один

Start (Вроде можно без предварительной сборки)

docker-compose up //Запуск всех трех
docker-compose run compute-intensity //Один

### Health check ###
GET /health


#### SSH tunneling ###
ssh -p 9007 root@samos.dozen.mephi.ru -L [local port]:localhost:[web service port]

