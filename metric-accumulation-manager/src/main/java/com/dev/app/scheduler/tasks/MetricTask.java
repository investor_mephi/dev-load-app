package com.dev.app.scheduler.tasks;

import com.dev.app.service.MetricsService;
import com.dev.app.web.PrometheusClient;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.DateTimeFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Service;

@Service
public class MetricTask implements Task {

    @Autowired
    private PrometheusClient prometheusClient;

    @Autowired
    private MetricsService metricsService;

    @Value("${cron.prometheus.metrics}")
    private String cron;

    @Override
    public void execute() {

        prometheusClient.getCpuLoadByQuery();
        //prometheusClient.getMemUsagePercentByQuery();
    }

    @Override
    public boolean isAppliable() {
        CronSequenceGenerator generator = new CronSequenceGenerator(cron);
        DateTime dateTime = new DateTime();
        DateTime cronTime = new DateTime(generator.next(dateTime.minusSeconds(1).toDate()));
        DateTimeComparator comparator = DateTimeComparator.getInstance(DateTimeFieldType.secondOfMinute());
        return comparator.compare(dateTime, cronTime) == 0;
    }
}
