package com.dev.app.scheduler.tasks;

public interface Task {

    void execute() throws Exception;

    boolean isAppliable();
}
