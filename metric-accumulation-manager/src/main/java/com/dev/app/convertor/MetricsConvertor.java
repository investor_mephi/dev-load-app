package com.dev.app.convertor;

import com.dev.app.Application;
import com.dev.app.model.MetricsEntity;
import com.dev.app.type.PrometheusRawObject;
import com.dev.app.type.PrometheusResultElement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class MetricsConvertor {

    public static MetricsEntity typeToModel(PrometheusResultElement e) {
        return new MetricsEntity(
                Application.useAutoIncrementedVal(),
                e.getMetric().get__name__(),
                e.getMetric().getInstance(),
                e.getMetric().getHost(),
                e.getValue().get(1).doubleValue(),
                e.getMetric().getContainer_name(),
                new Date(e.getValue().get(0).longValue() * 1000));

    }

    public static Collection<MetricsEntity> typeCollectionToModel(PrometheusRawObject prometheusRawObject) {
        Collection<PrometheusResultElement> elements = prometheusRawObject.getData().getResult();
        Collection<MetricsEntity> metricsEntities = new ArrayList<>(elements.size());
        elements.forEach(m -> metricsEntities.add(typeToModel(m)));
        return metricsEntities;
    }

}
