package com.dev.app.service;

import com.dev.app.convertor.MetricsConvertor;
import com.dev.app.model.MetricsEntity;
import com.dev.app.repository.MetricsRepository;
import com.dev.app.type.PrometheusRawObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MetricsService {

    @Autowired
    private MetricsRepository metricsRepository;

    public void saveMetrics(PrometheusRawObject prometheusRawObject) {
        Collection<MetricsEntity> metricsEntities = MetricsConvertor.typeCollectionToModel(prometheusRawObject);
        metricsEntities.forEach(m -> metricsRepository.insert(m));
    }
}
