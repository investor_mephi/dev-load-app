package com.dev.app.type;

public class PrometheusMetric {

    private String __name__;
    private String device;
    private String fstype;
    private String host;
    private String instance;
    private String job;
    private String mode;
    private String path;
    private String container_name;

    public String getContainer_name() {
        return container_name;
    }


    public String get__name__() {
        return __name__;
    }

    public String getDevice() {
        return device;
    }

    public String getFstype() {
        return fstype;
    }

    public String getHost() {
        return host;
    }

    public String getInstance() {
        return instance;
    }

    public String getJob() {
        return job;
    }

    public String getMode() {
        return mode;
    }

    public String getPath() {
        return path;
    }
}
