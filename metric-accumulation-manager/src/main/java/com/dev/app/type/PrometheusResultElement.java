package com.dev.app.type;

import java.util.List;

public class PrometheusResultElement {

    private PrometheusMetric metric;

    private List<Float> value;

    public PrometheusMetric getMetric() {
        return metric;
    }

    public void setMetric(PrometheusMetric metric) {
        this.metric = metric;
    }

    public List<Float> getValue() {
        return value;
    }

    public void setValue(List<Float> value) {
        this.value = value;
    }
}
