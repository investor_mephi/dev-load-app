package com.dev.app.type;

import java.util.List;

public class    PrometheusData {
    private String resultType;

    private List<PrometheusResultElement> result;

    public String getResultType() {
        return resultType;
    }

    public List<PrometheusResultElement> getResult() {
        return result;
    }
}
