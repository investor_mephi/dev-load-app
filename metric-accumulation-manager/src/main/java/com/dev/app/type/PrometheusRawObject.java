package com.dev.app.type;

public class PrometheusRawObject {
    private String status;
    private PrometheusData data;

    public String getStatus() {
        return status;
    }

    public PrometheusData getData() {
        return data;
    }
}
