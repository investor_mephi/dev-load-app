package com.dev.app.repository;

import com.dev.app.model.MetricsEntity;
import org.springframework.data.cassandra.repository.CassandraRepository;

public interface MetricsRepository extends CassandraRepository<MetricsEntity> {
}

