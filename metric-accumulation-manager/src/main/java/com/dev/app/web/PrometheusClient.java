package com.dev.app.web;
import com.dev.app.model.MetricsEntity;
import com.dev.app.repository.MetricsRepository;
import com.dev.app.service.MetricsService;
import com.dev.app.type.PrometheusRawObject;
import com.dev.app.util.PrometheusPropsWrapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;


@Service
public class PrometheusClient {

    @Autowired
    MetricsRepository repository;
    @Value("${prometheus.enabled}")
    private Boolean enabled;
    @Value("${prometheus.host}")
    private String host;
    @Value("${prometheus.port}")
    private String port;
    @Autowired
    private MetricsService metricsService;

    private Collection<MetricsEntity> getMetricByQuery(String query) {
        if (enabled) {
            HttpClient httpClient = HttpClientBuilder.create().build();

            HttpGet request = new HttpGet("http://localhost:9090/api/v1/query?query=" + query);
            try {
                HttpResponse response = httpClient.execute(request);
                String jsonProperty = new BufferedReader(new InputStreamReader(response.getEntity().getContent())).readLine();
                PrometheusRawObject prometheusRawObject = PrometheusPropsWrapper.wrap(jsonProperty);
                metricsService.saveMetrics(prometheusRawObject);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }

    public Collection<MetricsEntity> getMemUsagePercentByQuery() {
        return getMetricByQuery("avg_over_time(docker_container_mem_usage_percent[1m])");
    }

    public Collection<MetricsEntity> getCpuLoadByQuery() {
        return getMetricByQuery("avg_over_time(docker_container_cpu_usage_percent[1m])");
    }


}
