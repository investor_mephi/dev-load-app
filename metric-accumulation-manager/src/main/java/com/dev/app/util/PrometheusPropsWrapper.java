package com.dev.app.util;

import com.dev.app.type.PrometheusRawObject;
import com.google.gson.Gson;

public class PrometheusPropsWrapper {

    private static Gson gson = new Gson();

    public static PrometheusRawObject wrap(String json) {
        return gson.fromJson(json, PrometheusRawObject.class);
    }


}
