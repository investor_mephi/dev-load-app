package com.dev.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.atomic.AtomicLong;


@EnableScheduling
@SpringBootApplication
public class Application {
    private static AtomicLong autoIncrementedVal = new AtomicLong(1);

    public static long useAutoIncrementedVal() {
        return autoIncrementedVal.incrementAndGet();
    }


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
