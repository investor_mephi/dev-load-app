package com.dev.app.controller;

import com.dev.app.service.HybridService;
import com.dev.app.type.RequestParam;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("")
public class HybridController {

    @Autowired
    private HybridService hybridService;

    @RequestMapping(value = "/hybrid", method = RequestMethod.POST)
    public ResponseEntity<Void> dataLoad(@RequestBody RequestParam requestParam) throws NotFoundException, InterruptedException {
        if (requestParam == null || !requestParam.validation()) {
            throw new IllegalArgumentException();
        }
        hybridService.doHybrid(requestParam.getDuration(), requestParam.getThreadsCount(), requestParam.getComputingPercent());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleNotFoundException(
            Exception exception, HttpServletRequest request) {
        return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InterruptedException.class)
    public ResponseEntity<String> handleInterruptedException(
            Exception exception, HttpServletRequest request) {
        return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(
            Exception exception, HttpServletRequest request) {
        return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
