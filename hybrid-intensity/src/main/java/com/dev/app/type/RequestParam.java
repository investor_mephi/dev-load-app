package com.dev.app.type;

/**
 * Created by anna on 12.10.17.
 */
public class RequestParam {
    private Long duration;
    private Integer threadsCount;
    private Integer computingPercent;

    public RequestParam() {
    }

    public RequestParam(Long duration, Integer threadsCount, Integer computingPercent) {
        this.duration = duration;
        this.threadsCount = threadsCount;
        this.computingPercent = computingPercent;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Integer getThreadsCount() {
        return threadsCount;
    }

    public void setThreadsCount(Integer threadsCount) {
        this.threadsCount = threadsCount;
    }

    public Integer getComputingPercent() {
        return computingPercent;
    }

    public void setComputingPercent(Integer computingPercent) {
        this.computingPercent = computingPercent;
    }

    public Boolean validation() {
        if (duration != null && duration < 0) {
            return false;
        }
        if (threadsCount != null && threadsCount < 0) {
            return false;
        }
        if (computingPercent != null && (computingPercent < 0 || computingPercent > 100)) {
            return false;
        }
        return true;
    }
}