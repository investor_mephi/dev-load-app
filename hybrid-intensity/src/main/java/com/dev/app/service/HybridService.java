package com.dev.app.service;

public interface HybridService {

    void compute(Long duration, Integer threadsCount) throws InterruptedException;
    void dataLoad(Long duration, Integer threadsCount) throws InterruptedException;
    void doHybrid(Long duration, Integer threadsCount, Integer computingPercent) throws InterruptedException;
}
