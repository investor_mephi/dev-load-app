package com.dev.app.service;

import com.dev.app.dao.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class HybridServiceImpl implements HybridService {

    @Value("${DEFAULT_COUNT_RECORDS}")
    private Integer DEFAULT_COUNT_RECORDS;
    @Value("${DEFAULT_DURATION}")
    private Long DEFAULT_DURATION;
    @Value("${DEFAULT_THREAD_COUNT}")
    private Integer DEFAULT_THREAD_COUNT;
    @Value("${DEFAULT_COMPUTING_PERCENT}")
    private Integer DEFAULT_COMPUTING_PERCENT;


    @Autowired
    DataRepository dataRepository;

    @Override
    public void dataLoad(final Long duration, final Integer threadsCount) {

        ExecutorService executor = Executors.newFixedThreadPool(threadsCount);

        List<Runnable> workerThreadList = new LinkedList<>();
        for (int i = 0; i <= DEFAULT_COUNT_RECORDS; i++) {
            Runnable worker = new WorkerThread(new Long(i), dataRepository);
            workerThreadList.add(worker);
        }

        Long start = System.currentTimeMillis();

        while (!checkDuration(start, duration)) {
            for (Runnable w : workerThreadList) {
                executor.execute(w);
                if (checkDuration(start, duration)) {
                    break;
                }
            }
        }
        executor.shutdownNow();
    }

    private Boolean checkDuration(Long start, Long duration) {
        return ((System.currentTimeMillis() - start) >= duration * 1000);
    }

    @Override
    public void compute(Long duration, Integer threadsCount) {

        ExecutorService executorService = Executors.newFixedThreadPool(threadsCount);

        Collection<Runnable> tasks = new ArrayList<>();
        for (int i = 0; i < threadsCount; i++) {
            tasks.add(this::longCompute);
        }

        tasks.forEach(executorService::submit);

        new Thread(() -> {
            checkStatus(duration, executorService);
        }).start();


    }

    private void longCompute() {
        while (true) {
            if (isPrime()) {
                break;
            }
        }
    }

    private static boolean isPrime() {
        int n = 100000000;
        System.out.println("COMPUTE: " + Thread.currentThread().getName());
        for (int i = 2; i * i <= n; i++) {
            if (Thread.currentThread().isInterrupted()) {
                return true;
            }
            if (n % i == 0)
                return false;
        }
        return false;
    }

    private void checkStatus(Long duration, ExecutorService service) {
        Long startTime = System.currentTimeMillis();
        while (startTime + duration * 1000 > System.currentTimeMillis()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                //ignore
            }
        }
        service.shutdownNow();
    }

    @Override
    public void doHybrid(Long duration, Integer threadsCount, Integer computingPercent) throws InterruptedException {
        final Integer THREAD_COUNT = (threadsCount == null) ? DEFAULT_THREAD_COUNT : threadsCount;
        final Integer COMPUTING_PERCENT = (computingPercent == null) ? DEFAULT_COMPUTING_PERCENT : computingPercent;
        final Long DURATION = (duration == null) ? DEFAULT_DURATION : duration;

        final Integer THREAD_COUNT_COMPUTING = Math.round((float)COMPUTING_PERCENT/100 * THREAD_COUNT);
        final Integer THREAD_COUNT_DATA = THREAD_COUNT - THREAD_COUNT_COMPUTING;
//        final Long durationComputing =  Long.valueOf(Math.round((float)COMPUTING_PERCENT/100 * DURATION));
//        final Long durationData = DURATION - durationComputing;

        if (THREAD_COUNT_DATA > 0) {
            new Thread(() -> {
                dataLoad(DURATION, THREAD_COUNT_DATA);
            }).start();
        }


        if (THREAD_COUNT_COMPUTING > 0) {
            new Thread(() -> {
                compute(DURATION, THREAD_COUNT_COMPUTING);
            }).start();
        }

    }
}

class WorkerThread implements Runnable {
    private Long dataObjectId;
    private DataRepository dataRepository;

    public WorkerThread(Long dataObjectId, DataRepository dataRepository) {
        this.dataRepository = dataRepository;
        this.dataObjectId = dataObjectId;
    }

    @Override
    public void run() {
            dataRepository.updateById(new Long(dataObjectId));
            System.out.println("DATA: " + Thread.currentThread().getName());
    }
}
