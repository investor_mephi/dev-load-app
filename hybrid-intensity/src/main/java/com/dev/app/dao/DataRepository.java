package com.dev.app.dao;

import com.dev.app.actuator.service.Repository;
import com.dev.app.domain.DataObject;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface DataRepository extends CrudRepository<DataObject, Long>, Repository {

    @Transactional
    @Modifying
    @Query("UPDATE DataObject d " +
            "SET d.field1='1111111', " +
                "d.field2='2222222', " +
                "d.field3='3333333', " +
                "d.field4='4444444', " +
                "d.field5='5555555' " +
            "WHERE d.id = :id")
    void updateById(@Param("id") Long id);

    @Query(value = "SELECT version()", nativeQuery = true)
    String check();
}
