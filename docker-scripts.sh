#!/bin/bash
One liner to stop / remove all of Docker containers:
# Stop all containers
docker stop $(docker ps -a -q)
# Delete all containers
docker rm -f $(docker ps -a -q)
# Delete all images
docker rmi -f $(docker images -q)
