#!/bin/bash

IP=$1
PORT=$2
PASSWORD=$3
IMAGE_NAME=$4
CONTAINER_NAME=$5
IP_DEST=$6
PORT_DEST=$7
PASSWORD_DEST=$8
CONTAINER_PORT_AND_NAME_POSTFIX=$9


#Уважительно просим контейнер остановиться
sshpass -p $PASSWORD ssh -p $PORT root@$IP docker stop $CONTAINER_NAME
echo "container stopped\n"

#Нежно сохраняем образ контейнера
sshpass -p $PASSWORD ssh -p $PORT root@$IP docker save -o /root/dockerContainers/$IMAGE_NAME  $IMAGE_NAME
echo "container saved\n"

sshpass -p $PASSWORD ssh -p $PORT root@$IP sshpass -p $PASSWORD_DEST scp /root/dockerContainers/$IMAGE_NAME root@$IP_DEST:/root/dockerContainers
echo "container passed\n"

sshpass -p $PASSWORD_DEST ssh -v -o ConnectTimeout=1 -p $PORT_DEST root@$IP_DEST docker run -i --name experiment-computing-$9 -p $9:8086 computing:spring
echo "container running\n"