
DROP TABLE IF EXISTS Stat;
DROP TABLE IF EXISTS Vnf;
DROP TABLE IF EXISTS Configuration;
DROP TABLE IF EXISTS Slot;
DROP TABLE IF EXISTS Chain;
DROP TABLE IF EXISTS Catalog;
DROP TABLE IF EXISTS Host;


CREATE TABLE Host(
  id BIGSERIAL PRIMARY KEY,
  ip_address VARCHAR(30) NOT NULL,
  ssh_port BIGINT NOT NULL,
  root_password VARCHAR(250) NOT NULL

);

CREATE TABLE Catalog(
    id BIGSERIAL PRIMARY KEY,
    type VARCHAR(30) NOT NULL

);

CREATE TABLE Chain(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(100) NOT NULL

);

CREATE TABLE Slot(
  id BIGSERIAL PRIMARY KEY,
  cpu_count BIGINT NOT NULL,
  gpu_count BIGINT NOT NULL,
  host_id BIGINT NOT NULL,
  CONSTRAINT FK_Slot_HostId FOREIGN KEY (host_id) REFERENCES Host (id)

);


CREATE TABLE Configuration(
  id BIGSERIAL PRIMARY KEY,
  gpu_enable BOOLEAN DEFAULT FALSE,
  cuda_block_size BIGINT NOT NULL,
  cuda_shmem_size BIGINT NOT NULL,
  cuda_sm_count BIGINT NOT NULL,
  buffer_size BIGINT NOT NULL,
  vnf_next_ip VARCHAR(30),
  vnf_next_port BIGINT,
  vnf_last BOOLEAN
  );

CREATE TABLE Vnf(
  id BIGSERIAL PRIMARY KEY,
  vm_name VARCHAR(100) NOT NULL,
  ip_address VARCHAR(30) NOT NULL,
  ssh_port BIGINT NOT NULL,
  rest_port BIGINT NOT NULL,
  state BIGINT NOT NULL,
  slot_id BIGINT NOT NULL,
  catalog_id BIGINT NOT NULL,
  configuration_id BIGINT NOT NULL,
  CONSTRAINT FK_Vnf_SlotId FOREIGN KEY (slot_id) REFERENCES Slot (id),
  CONSTRAINT FK_Vnf_CatalogId FOREIGN KEY (catalog_id) REFERENCES Catalog (id),
  CONSTRAINT FK_Vnf_ConfigurationId FOREIGN KEY (configuration_id) REFERENCES Configuration (id)
);

CREATE TABLE Stat(
  id BIGSERIAL PRIMARY KEY,
  created_at timestamp  NOT NULL,
  json TEXT NOT NULL,
  vnf_id BIGINT NOT NULL,
  CONSTRAINT FK_Stat_VnfId FOREIGN KEY (vnf_id) REFERENCES Vnf (id)

);