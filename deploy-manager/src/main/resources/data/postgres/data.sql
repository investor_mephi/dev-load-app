



INSERT INTO Host(ip_address, ssh_port, root_password) VALUES ('192.168.12.84',22,'dozenhpc');
--INSERT INTO Host(ip_address, ssh_port, root_password) VALUES ('192.168.12.61',22,'dozenhpc');
INSERT INTO Host(ip_address, ssh_port, root_password) VALUES ('unknown',0,'unknown');


INSERT INTO Catalog(type) VALUES ('encryption');
INSERT INTO Catalog(type) VALUES ('compression');


INSERT INTO Chain(name) VALUES ('test');


INSERT INTO Slot(cpu_count, gpu_count, host_id) VALUES (1,0,1);
INSERT INTO Slot(cpu_count, gpu_count, host_id) VALUES (1,1,2);

INSERT INTO Configuration(gpu_enable, cuda_block_size, cuda_shmem_size, cuda_sm_count, buffer_size, vnf_next_ip, vnf_next_port, vnf_last)
VALUES (FALSE, 256, 48000, 2, 4096, '127.0.0.1', 8880, true);


INSERT INTO Vnf(vm_name, ip_address, ssh_port, rest_port, state, slot_id, catalog_id, configuration_id)
 VALUES ('unknown','127.0.0.1', 22, 8080, 5, 2, 1, 1);