package com.manage.controllers;

import com.manage.service.MainService;
import com.manage.type.DeployMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

@RestController
public class MainController {

    @Autowired
    private MainService mainService;

    /**
     *
     * Тестовый JSON для тела запроса{
     "ip":"samos.dozen.mephi.ru",
     "port":9028,
     "password":"dozenhpc",

     "imageName":"data:spring",
     "containerName":"devloadapp_hybrid-intensity_1",

     "ipDest":"samos.dozen.mephi.ru",
     "portDest":9029,
     "passwordDest":"dozenhpc",
     "ipDestInInternalNetwork":"192.168.12.89"
     }
     * @param deployMessage
     * @return
     */
    @RequestMapping(
            value="/deploy",
            consumes =  MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST
    )
    public ResponseEntity<Object> deployVnf(@RequestBody DeployMessage deployMessage){
        try{
            mainService.deployVnf(deployMessage);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * Тестовый JSON для тела запроса[{
     "ip":"samos.dozen.mephi.ru",
     "port":9028,
     "password":"dozenhpc",

     "imageName":"data:spring",
     "containerName":"devloadapp_hybrid-intensity_1",

     "ipDest":"samos.dozen.mephi.ru",
     "portDest":9029,
     "passwordDest":"dozenhpc",
     "ipDestInInternalNetwork":"192.168.12.89"
     }
     ]
     * @param deployMessageCollection
     * @return
     */
    @RequestMapping(
            value="/deployMany",
            consumes =  MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST
    )
    public ResponseEntity<Object> deployVnf(@RequestBody Collection<DeployMessage> deployMessageCollection){
        try{
            mainService.deployVnf(deployMessageCollection);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value="/deployManyByNumber",
            method = RequestMethod.GET
    )
    public ResponseEntity<Object> deployVnf(@RequestParam(value = "number", required = false) long number){
        Collection<DeployMessage> deployMessageCollection = new ArrayList();
        String ip ="samos.dozen.mephi.ru";
        Long port = 9028L;
        String password = "dozenhpc";
        String imageName = "computing:spring";

        String ipDest = "samos.dozen.mephi.ru";
        Long portDest = 9029L;
        String passwordDest="dozenhpc";
        String ipDestInInternalNetwork = "192.168.12.89";
        for(int i = 0; i < number; i++){
            String curJson = "experiment-computing-dozenhpc-" + (i+1);
            DeployMessage deployMessage= new DeployMessage(ip,port,password,imageName,
                    curJson,
                    ipDest,portDest,passwordDest,ipDestInInternalNetwork);
            deployMessageCollection.add(deployMessage);
        }
        try{
            mainService.deployVnf(deployMessageCollection);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
