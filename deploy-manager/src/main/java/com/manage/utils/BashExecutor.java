package com.manage.utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BashExecutor {

    private static String[] concat(String[] a, String[] b) {
        int aLen = a.length;
        int bLen = b.length;
        String[] c= new String[aLen+bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    public static String executeCommand(String command){
        String res = "";

        String[] cmd = {command};
        try {
            Process pb = Runtime.getRuntime().exec(cmd);


            BufferedReader input = new BufferedReader(new InputStreamReader(pb.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                //System.out.println(line);
                res += line;
                res += "\n";
            }
            input.close();
        }
        catch (Exception e){e.printStackTrace();}

        return res;
    }

    public static String executeScript(String file, String[] params) {
        String res = "";

        String[] cmd = {"/bin/bash", file};
        cmd = concat(cmd, params);
        try {
            Process pb = Runtime.getRuntime().exec(cmd);


            BufferedReader input = new BufferedReader(new InputStreamReader(pb.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                //System.out.println(line);
                res += line;
                res += "\n";
            }
            input.close();
        }
        catch (Exception e){e.printStackTrace();}

        return res;
    }


    public static void main(String[] args) throws IOException {

        System.out.print(executeScript("deployVNF.sh", new String[] {}));

    }
}
