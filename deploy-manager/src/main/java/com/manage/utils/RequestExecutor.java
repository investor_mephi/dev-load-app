package com.manage.utils;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

public class RequestExecutor {

    public static HttpResponse makeRequest(String method, String url, String body){

        HttpRequestBase requestBase;
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {


            StringEntity entity =new StringEntity(body);
            switch (method) {
                case "GET": requestBase = new HttpGet(url);
                    break;
                case "POST": requestBase = new HttpPost(url);
                    ((HttpPost) requestBase).setEntity(entity);
                    break;
                case "PUT": requestBase = new HttpPut(url);
                    ((HttpPut) requestBase).setEntity(entity);
                    break;
                case "DELETE": requestBase = new HttpDeleteWithBody(url);
                    ((HttpDeleteWithBody) requestBase).setEntity(entity);
                    break;
                default: throw new IllegalArgumentException();
            }


            requestBase.addHeader("Content-Type", "application/json");
            HttpResponse response = httpClient.execute(requestBase);
            return response;


        }catch (Exception ex) {
            return null;
        }

    }

    public static void main(String[] args){
        HttpResponse response = makeRequest("GET", "http://ya.ru", "");
        response.getStatusLine().getStatusCode();
    }
}
