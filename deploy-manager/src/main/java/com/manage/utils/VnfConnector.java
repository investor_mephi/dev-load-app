package com.manage.utils;


import java.io.IOException;

public class VnfConnector {

    public static void deployVnf(String ip, String rootPassword) throws IOException {

        BashExecutor.executeScript("deployVNF.sh", new String[] {ip, rootPassword});

    }

    public static void main(String[] args){

    }
}
