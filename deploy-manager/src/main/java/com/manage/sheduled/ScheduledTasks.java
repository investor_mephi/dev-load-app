package com.manage.sheduled;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.manage.model.ConfigurationEntity;
import com.manage.model.StatEntity;
import com.manage.model.VnfEntity;
import com.manage.repository.StatRepository;
import com.manage.repository.VnfRepository;
import com.manage.type.ManageMessage;
import com.manage.utils.BashExecutor;
import com.manage.utils.RequestExecutor;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

//    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);
//
//    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
//
//    @Scheduled(fixedRate = 5000)
//    public void reportCurrentTime() {
//        log.info("The time is now {}", dateFormat.format(new Date()));
//    }

    @Autowired
    private VnfRepository vnfRepository;

    @Autowired
    private StatRepository statRepository;


    @Scheduled(fixedRate = 4000)
    public void checkVnf() throws Exception {

        Collection<VnfEntity> vnfEntities = vnfRepository.findAll();
        for (VnfEntity vnfEntity: vnfEntities){
            String url = "http://" + vnfEntity.getIpAddress() + ":" + vnfEntity.getRestPort() + "/check";
            HttpResponse httpResponse = RequestExecutor.makeRequest("GET", url,"");
                if (httpResponse != null && httpResponse.getStatusLine().getStatusCode() == 204) {
                    if (vnfEntity.getState() == 5L){
                        String url1 = "http://" + vnfEntity.getIpAddress() + ":" + vnfEntity.getRestPort() + "/config/vnf";
                        ConfigurationEntity configurationEntity = vnfEntity.getConfigurationEntity();
                        ManageMessage manageMessage = new ManageMessage(configurationEntity.getGpuEnable(),
                                configurationEntity.getCudaBlockSize().intValue(), configurationEntity.getCudaShmemSize().intValue(),
                                configurationEntity.getCudaSmCount().intValue(), configurationEntity.getBufferSize().intValue(),
                                configurationEntity.getVnfNextIp(), configurationEntity.getVnfNextPort().intValue(), configurationEntity.getVnfLast());
                        ObjectMapper mapper = new ObjectMapper();
                        String body = mapper.writeValueAsString(manageMessage);
                        HttpResponse httpResponse1 = RequestExecutor.makeRequest("POST", url1, body);
                    }
                    vnfEntity.setState(0L);
                    vnfRepository.save(vnfEntity);
                }
                else {
                    Long state = vnfEntity.getState();
                    if (state == 0L || state == 1L){
                        state++;
                        vnfEntity.setState(state);
                        vnfRepository.save(vnfEntity);
                    } else if (state == 2L){
                        // перезапуск vnf
                        // BashExecutor.executeScript("restartVNF.sh", new String[] {vnfEntity.getSlotEntity().getHostEntity().getIpAddress(),
                        //       Long.toString(vnfEntity.getSlotEntity().getHostEntity().getSshPort()),
                        //       vnfEntity.getSlotEntity().getHostEntity().getRootPassword(), vnfEntity.getVmName(), vnfEntity.getIpAddress(), Long.toString(vnfEntity.getSshPort())});
                        state = 5L;
                        vnfEntity.setState(state);
                        vnfRepository.save(vnfEntity);
                    }
                }
        }

    }

    @Scheduled(fixedRate = 60000)
    public void getStatistic(){
        Collection<VnfEntity> vnfEntities = vnfRepository.findAll();
        for (VnfEntity vnfEntity : vnfEntities){
            String url = "http://" + vnfEntity.getIpAddress() + ":" + vnfEntity.getRestPort() + "/metrics";
            HttpResponse httpResponse = RequestExecutor.makeRequest("GET", url,"");
            if (httpResponse != null && httpResponse.getStatusLine().getStatusCode() == 200){
                String json;
                try {
                    json = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");

                    StatEntity statEntity = new StatEntity(null, new java.util.Date(System.currentTimeMillis()), json, vnfEntity);
                    statRepository.save(statEntity);
                }
                catch (Exception e) {e.printStackTrace();}
            }
        }
    }
}
