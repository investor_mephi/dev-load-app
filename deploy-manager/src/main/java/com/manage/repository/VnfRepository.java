package com.manage.repository;


import com.manage.model.VnfEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VnfRepository extends JpaRepository<VnfEntity, Long> {
}
