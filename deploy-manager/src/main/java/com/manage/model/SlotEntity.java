package com.manage.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Slot")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class SlotEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Type(type = "")
    private Long id;

    @Column(name = "cpu_count")
    private Long cpuCount;

    @Column(name = "gpu_count")
    private Long gpuCount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "host_id")
    private HostEntity hostEntity;

    @OneToMany(mappedBy = "slotEntity", fetch = FetchType.EAGER)
    private Set<VnfEntity> vnfEntitySet;

    public SlotEntity(){}

    public SlotEntity(Long id, Long cpuCount, Long gpuCount, HostEntity hostEntity, Set<VnfEntity> vnfEntities){
        this.id = id;
        this.cpuCount = cpuCount;
        this.gpuCount = gpuCount;
        this.hostEntity = hostEntity;
        this.vnfEntitySet = vnfEntities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCpuCount() {
        return cpuCount;
    }

    public void setCpuCount(Long cpuCount) {
        this.cpuCount = cpuCount;
    }

    public Long getGpuCount() {
        return gpuCount;
    }

    public void setGpuCount(Long gpuCount) {
        this.gpuCount = gpuCount;
    }

    public HostEntity getHostEntity() {
        return hostEntity;
    }

    public void setHostEntity(HostEntity hostEntity) {
        this.hostEntity = hostEntity;
    }

    public Set<VnfEntity> getVnfEntitySet() {
        return vnfEntitySet;
    }

    public void setVnfEntitySet(Set<VnfEntity> vnfEntitySet) {
        this.vnfEntitySet = vnfEntitySet;
    }
}
