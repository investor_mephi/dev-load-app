package com.manage.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "Configuration")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ConfigurationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Type(type = "")
    private Long id;

    @Column(name = "gpu_enable")
    private Boolean gpuEnable;

    @Column(name = "cuda_block_size")
    private Long cudaBlockSize;

    @Column(name = "cuda_shmem_size")
    private Long cudaShmemSize;

    @Column(name = "cuda_sm_count")
    private Long cudaSmCount;

    @Column(name = "buffer_size")
    private Long bufferSize;

    @Column(name = "vnf_next_ip")
    private String vnfNextIp;

    @Column(name = "vnf_next_port")
    private Long vnfNextPort;

    @Column(name = "vnf_last")
    private Boolean vnfLast;


    public ConfigurationEntity(){}

    public ConfigurationEntity(Boolean gpuEnable, Long cudaBlockSize, Long cudaShmemSize, Long cudaSmCount, Long bufferSize, String vnfNextIp,
                               Long vnfNextPort, Boolean vnfLast){
        this.id = null;
        this.gpuEnable = gpuEnable;
        this.cudaBlockSize = cudaBlockSize;
        this.cudaShmemSize = cudaShmemSize;
        this.cudaSmCount = cudaSmCount;
        this.bufferSize = bufferSize;
        this.vnfNextIp = vnfNextIp;
        this.vnfNextPort = vnfNextPort;
        this.vnfLast = vnfLast;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getGpuEnable() {
        return gpuEnable;
    }

    public void setGpuEnable(Boolean gpuEnable) {
        this.gpuEnable = gpuEnable;
    }

    public Long getCudaBlockSize() {
        return cudaBlockSize;
    }

    public void setCudaBlockSize(Long cudaBlockSize) {
        this.cudaBlockSize = cudaBlockSize;
    }

    public Long getCudaShmemSize() {
        return cudaShmemSize;
    }

    public void setCudaShmemSize(Long cudaShmemSize) {
        this.cudaShmemSize = cudaShmemSize;
    }

    public Long getCudaSmCount() {
        return cudaSmCount;
    }

    public void setCudaSmCount(Long cudaSmCount) {
        this.cudaSmCount = cudaSmCount;
    }

    public Long getBufferSize() {
        return bufferSize;
    }

    public void setBufferSize(Long bufferSize) {
        this.bufferSize = bufferSize;
    }

    public String getVnfNextIp() {
        return vnfNextIp;
    }

    public void setVnfNextIp(String vnfNextIp) {
        this.vnfNextIp = vnfNextIp;
    }

    public Long getVnfNextPort() {
        return vnfNextPort;
    }

    public void setVnfNextPort(Long vnfNextPort) {
        this.vnfNextPort = vnfNextPort;
    }

    public Boolean getVnfLast() {
        return vnfLast;
    }

    public void setVnfLast(Boolean vnfLast) {
        this.vnfLast = vnfLast;
    }

}
