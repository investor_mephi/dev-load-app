package com.manage.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "Vnf")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class VnfEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Type(type = "")
    private Long id;

    @Column(name = "vm_name")
    private String vmName;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "ssh_port")
    private Long sshPort;

    @Column(name = "rest_port")
    private Long restPort;

    @Column(name = "state")
    private Long state; // состояние vnf.  5 - только добавлена либо восстановлена, 0 - работает, 1 - 1 раз не откликнулась, 2 - 2 раза, 3 - 3 раза

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "slot_id")
    private SlotEntity slotEntity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "catalog_id")
    private CatalogEntity catalogEntity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "configuration_id")
    private ConfigurationEntity configurationEntity;

    public VnfEntity(){}

    public VnfEntity(Long id, String vmName, String ipAddress, Long sshPort, Long restPort, Long state, SlotEntity slotEntity,
                     CatalogEntity catalogEntity, ConfigurationEntity configurationEntity){
        this.id = id;
        this.vmName = vmName;
        this.ipAddress = ipAddress;
        this.sshPort = sshPort;
        this.restPort = restPort;
        this.state = state;
        this.slotEntity = slotEntity;
        this.catalogEntity = catalogEntity;
        this.configurationEntity = configurationEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVmName() {
        return vmName;
    }

    public void setVmName(String vmName) {
        this.vmName = vmName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Long getSshPort() {
        return sshPort;
    }

    public void setSshPort(Long sshPort) {
        this.sshPort = sshPort;
    }

    public Long getRestPort() {
        return restPort;
    }

    public void setRestPort(Long restPort) {
        this.restPort = restPort;
    }

    public Long getState() {
        return state;
    }

    public void setState(Long state) {
        this.state = state;
    }

    public SlotEntity getSlotEntity() {
        return slotEntity;
    }

    public void setSlotEntity(SlotEntity slotEntity) {
        this.slotEntity = slotEntity;
    }

    public CatalogEntity getCatalogEntity() {
        return catalogEntity;
    }

    public void setCatalogEntity(CatalogEntity catalogEntity) {
        this.catalogEntity = catalogEntity;
    }

    public ConfigurationEntity getConfigurationEntity() {
        return configurationEntity;
    }

    public void setConfigurationEntity(ConfigurationEntity configurationEntity) {
        this.configurationEntity = configurationEntity;
    }
}
