package com.manage.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "Host")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class HostEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Type(type = "")
    private Long id;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "root_password")
    private String rootPassword;

    @Column(name = "ssh_port")
    private Long sshPort;

    public HostEntity(){}

    public HostEntity(Long id, String ipAddress, String rootPassword, Long sshPort){
        this.id = id;
        this.ipAddress = ipAddress;
        this.rootPassword = rootPassword;
        this.sshPort = sshPort;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getRootPassword() {
        return rootPassword;
    }

    public void setRootPassword(String rootPassword) {
        this.rootPassword = rootPassword;
    }

    public Long getSshPort() {
        return sshPort;
    }

    public void setSshPort(Long sshPort) {
        this.sshPort = sshPort;
    }
}
