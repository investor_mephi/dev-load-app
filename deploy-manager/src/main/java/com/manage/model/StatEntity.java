package com.manage.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Stat")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class StatEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Type(type = "")
    private Long id;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "json")
    private String json;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "vnf_id")
    private VnfEntity vnfEntity;

    public StatEntity(){}

    public StatEntity(Long id, Date createdAt, String json, VnfEntity vnfEntity){
        this.id = id;
        this.createdAt = createdAt;
        this.json = json;
        this.vnfEntity = vnfEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public VnfEntity getVnfEntity() {
        return vnfEntity;
    }

    public void setVnfEntity(VnfEntity vnfEntity) {
        this.vnfEntity = vnfEntity;
    }
}
