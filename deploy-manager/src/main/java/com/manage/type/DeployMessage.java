package com.manage.type;


public class DeployMessage {

    private String ip;

    private Long port;

    private String password;

    private String imageName;

    private String containerName;

    private String ipDest;

    private Long portDest;

    private String passwordDest;

    private String ipDestInInternalNetwork;

    public DeployMessage() {
    }

    public DeployMessage(String ip, Long port, String password, String imageName, String containerName, String ipDest, Long portDest, String passwordDest, String ipDestInInternalNetwork) {
        this.ip = ip;
        this.port = port;
        this.password = password;
        this.imageName = imageName;
        this.containerName = containerName;
        this.ipDest = ipDest;
        this.portDest = portDest;
        this.passwordDest = passwordDest;
        this.ipDestInInternalNetwork = ipDestInInternalNetwork;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getPort() {
        return port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getIpDest() {
        return ipDest;
    }

    public void setIpDest(String ipDest) {
        this.ipDest = ipDest;
    }

    public Long getPortDest() {
        return portDest;
    }

    public void setPortDest(Long portDest) {
        this.portDest = portDest;
    }

    public String getPasswordDest() {
        return passwordDest;
    }

    public void setPasswordDest(String passwordDest) {
        this.passwordDest = passwordDest;
    }

    public String getIpDestInInternalNetwork() {
        return ipDestInInternalNetwork;
    }

    public void setIpDestInInternalNetwork(String ipDestInInternalNetwork) {
        this.ipDestInInternalNetwork = ipDestInInternalNetwork;
    }
}
