package com.manage.type;


public class ManageMessage {

    private Boolean gpuEnable;

    private Integer cudaBlockSize;

    private Integer cudaShmemSize;

    private Integer cudaSmCount;

    private Integer bufferSize;

    private String nextIp;

    private Integer nextPort;

    private Boolean last;

    public ManageMessage(){}

    public ManageMessage(Boolean gpuEnable, Integer cudaBlockSize, Integer cudaShmemSize, Integer cudaSmCount, Integer bufferSize, String nextIp, Integer nextPort, Boolean last){

        this.gpuEnable = gpuEnable;
        this.cudaBlockSize = cudaBlockSize;
        this.cudaShmemSize = cudaShmemSize;
        this.cudaSmCount = cudaSmCount;
        this.bufferSize = bufferSize;
        this.nextIp = nextIp;
        this.nextPort = nextPort;
        this.last = last;
    }

    public Boolean getGpuEnable() {
        return gpuEnable;
    }

    public void setGpuEnable(Boolean gpuEnable) {
        this.gpuEnable = gpuEnable;
    }

    public Integer getCudaBlockSize() {
        return cudaBlockSize;
    }

    public void setCudaBlockSize(Integer cudaBlockSize) {
        this.cudaBlockSize = cudaBlockSize;
    }

    public Integer getCudaShmemSize() {
        return cudaShmemSize;
    }

    public void setCudaShmemSize(Integer cudaShmemSize) {
        this.cudaShmemSize = cudaShmemSize;
    }

    public Integer getCudaSmCount() {
        return cudaSmCount;
    }

    public void setCudaSmCount(Integer cudaSmCount) {
        this.cudaSmCount = cudaSmCount;
    }

    public Integer getBufferSize() {
        return bufferSize;
    }

    public void setBufferSize(Integer bufferSize) {
        this.bufferSize = bufferSize;
    }

    public String getNextIp(){return nextIp;}

    public void setNextIp(String nextIp){this.nextIp = nextIp;}

    public Integer getNextPort(){return nextPort;}

    public void setNextPort(Integer nextPort){this.nextPort = nextPort;}

    public Boolean getLast(){return last;}

    public void setLast(Boolean last){this.last = last;}
}
