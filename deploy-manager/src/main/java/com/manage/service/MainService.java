package com.manage.service;

import com.manage.model.CatalogEntity;
import com.manage.model.ConfigurationEntity;
import com.manage.model.SlotEntity;
import com.manage.model.VnfEntity;
import com.manage.repository.CatalogRepository;
import com.manage.repository.ConfigurationRepository;
import com.manage.repository.SlotRepository;
import com.manage.repository.VnfRepository;
import com.manage.type.DeployMessage;
import com.manage.utils.BashExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class MainService {

    @Autowired
    private SlotRepository slotRepository;

    @Autowired
    private CatalogRepository catalogRepository;

    @Autowired
    private ConfigurationRepository configurationRepository;

    @Autowired
    private VnfRepository vnfRepository;

    //Резервирование портов с 50001 по любой другой для определённости в работе скрипта
    private Integer containerPortCounter = 50001;

    public void deployVnf(DeployMessage deployMessage) {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("deployContainer.sh").getFile());
        int portForContainer = containerPortCounter++;
        new Thread(() -> {
            String res = BashExecutor.executeScript(file.getAbsolutePath(), new String[]{deployMessage.getIp(),
                    deployMessage.getPort().toString(), deployMessage.getPassword(),
                    deployMessage.getImageName(), deployMessage.getContainerName(),
                    deployMessage.getIpDest(), deployMessage.getPortDest().toString(),
                    deployMessage.getPasswordDest(), String.valueOf(portForContainer)});
        }).start();
    }

    public void deployVnf(Collection<DeployMessage> deployMessages) {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("deployContainer.sh").getFile());
        for (DeployMessage deployMessage : deployMessages) {
            int portForContainer = containerPortCounter++;
            new Thread(() -> {
                String res = BashExecutor.executeScript(file.getAbsolutePath(), new String[]{deployMessage.getIp(),
                        deployMessage.getPort().toString(), deployMessage.getPassword(),
                        deployMessage.getImageName(), deployMessage.getContainerName(),
                        deployMessage.getIpDest(), deployMessage.getPortDest().toString(),
                        deployMessage.getPasswordDest(), String.valueOf(portForContainer)});
            }).start();
        }
    }
}


//
//    public void deployVnf(DeployMessage deployMessage) {
//        String res0=BashExecutor.executeScript("deployContainer.sh", new String[]{"samos.dozen.mephi.ru",
//                Long.toString(9028), "dozenhpc", "computing:spring", "elastic_bose", "samos.dozen.mephi.ru",
//                Long.toString(9029), "dozenhpc"});
//        String res = BashExecutor.executeScript("deployContainer.sh", new String[]{deployMessage.getIp(),
//                deployMessage.getPort().toString(), deployMessage.getPassword
//                (),
//                deployMessage.getImageName(), deployMessage.getContainerName(),
//                deployMessage.getIpDest(), deployMessage.getPortDest().toString(),
//                deployMessage.getPasswordDest(), deployMessage.getIpDestInInternalNetwork()});
//    }
//}
/*
BashExecutor.executeScript("deployContainer.sh", new String[]{"samos.dozen.mephi.ru",
                    Long.toString(9028), "dozenhpc", "computing:spring", "elastic_bose", "samos.dozen.mephi.ru",
                    Long.toString(9029), "dozenhpc", "192.168.12.89"});
 */