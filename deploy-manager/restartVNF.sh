#!/bin/bash

IP=$1
PORT=$2
PASS=$3
VMNAME=$4
VMIP=$5
VMPORT=$6

sshpass -p $PASS ssh -p $PORT root@$IP virsh destroy $VMNAME
sshpass -p $PASS ssh -p $PORT root@$IP virsh start $VMNAME
sleep 1m

sshpass -p dozenhpc ssh -p $VMPORT root@$VMIP service postgresql start
sshpass -p dozenhpc ssh -p $VMPORT root@$VMIP java -jar NVF/aes.jar